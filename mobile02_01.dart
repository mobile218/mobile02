import 'dart:io';

bool isPrime(Num) {
  for (var i = 2; i <= Num / i; ++i) {
    if (Num % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print('Enter Num');
  var Num = int.parse(stdin.readLineSync()!);
  if (isPrime(Num)) {
    print('$Num is a prime number.');
  } else {
    print('$Num is not a prime number.');
  }
}
